package org.linegate.healthtone

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class AppBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        when (intent?.action) {
            Intent.ACTION_BOOT_COMPLETED -> {
                val stepsServiceIntent = Intent(context, StepsService::class.java)
                stepsServiceIntent.action = Constants.actionStartForegroundService
                context?.startForegroundService(stepsServiceIntent)
            }
        }
    }
}