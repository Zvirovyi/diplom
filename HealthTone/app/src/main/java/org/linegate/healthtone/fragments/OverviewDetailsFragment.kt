package org.linegate.healthtone.fragments

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import org.linegate.healthtone.AddDialog
import org.linegate.healthtone.R
import org.linegate.healthtone.appviewmodel.AppViewModel
import org.linegate.healthtone.databinding.FragmentOverviewDetailsBinding

class OverviewDetailsFragment : Fragment() {
    private var _binding: FragmentOverviewDetailsBinding? = null
    val binding
        get() = _binding!!

    private val appViewModel: AppViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return inflater.inflate(R.layout.fragment_main, container, false)
        _binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_overview_details, container, false)
        binding.appViewModel = appViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}