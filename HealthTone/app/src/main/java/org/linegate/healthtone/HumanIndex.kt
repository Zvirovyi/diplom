package org.linegate.healthtone

import kotlin.math.roundToInt
import kotlin.math.sqrt

object HumanIndex {

    val normalBMIDeviation = 3f

    /**
     * @param[personBMI] Person's BMI.
     * @param[normalBMI] Normal BMI for person.
     * @return BMI difference.
     */
    fun getBMIDiff(personBMI: Float, normalBMI: Float): Float {
        return personBMI - normalBMI
    }

    /**
     * @param[height] Person's height in cm.
     * @param[weight] Person's weight in cm.
     * @return Calculated person's BMI.
     */
    fun getBMI(height: Int, weight: Int): Float {
        return weight / Math.pow(height / 100.0, 2.0).toFloat()
    }

    /**
     * @param[age] Person's age.
     * @return Standart body mass index for specified age.
     */
    fun getNormalBMI(age: Int) : Float {
        if (age <= 8) {
            return 16f
        } else if (age >= 20) {
            return 22f
        } else {
            return 16f + (age - 8f) * 0.5f
        }
    }

    /**
     * @param[age] Person's age.
     * @param[weight] Person's weight for more accuracy calculation with BMI in kg. If not specified, standart height is used.
     * @return Calculated person's height in cm.
     */
    fun getNormalHeight(age: Int, weight: Int? = null) : Int {
        weight?.let {
            return (sqrt(weight / getNormalBMI(age)) * 100).roundToInt()
        }
        if (age >= 20) {
            return 170
        } else {
            return (80 + (age - 1) * ((170 - 80) / 19f)).roundToInt()
        }
    }

    /**
     * @param[age] Person's age.
     * @param[height] Person's height for more accuracy calculation with BMI in cm. If not specified, standart weight is used.
     * @return Calculated person's weight in kg.
     */
    fun getNormalWeight(age: Int, height: Int? = null) : Int {
        height?.let {
            return (getNormalBMI(age) * Math.pow(height / 100.0, 2.0)).roundToInt()
        }
        return (getNormalBMI(age) * Math.pow(getNormalHeight(age) / 100.0, 2.0)).roundToInt()
    }

    /**
     * @return Daily steps count norm for person.
     */
    fun getDailyStepsNorm(): Int {
        return 10_000
    }

    /**
     * @param[stepsCount] Person's steps count.
     * @return Person's activity coefficient based on steps count.
     */
    fun getActivityCoefficientBySteps(stepsCount: Int): Float {
        return 1.45f + (stepsCount - getDailyStepsNorm()) * 0.000025f
    }

    /**
     * @param[age] Person's age.
     * @return Calculated daily sleep norm for person in hours.
     */
    fun getDailySleepNorm(age: Int): Int {
        if (age <= 12) {
            return 10
        } else if (age <= 18) {
            return 9
        } else if (age <= 40) {
            return 8
        } else {
            return 7
        }
    }

    /**
     * @param[weight] Person's weight for calculation in kg.
     * @param[bmiDiff] Difference between person's BMI and standart BMI for his age.
     * @return Calculated daily water norm for person in ml.
     */
    fun getDailyWaterNorm(weight: Int, bmiDiff: Float): Int {
        if (bmiDiff > normalBMIDeviation) {
            return weight * 30
        }
        return weight * 25
    }

    /**
     * @param[age] Person's age.
     * @param[height] Person's height in cm.
     * @param[weight] Person's weight in kg.
     * @param[activityCoefficient] Person's activity between 1.2(no activity) and 1.9(professional sport). Standart - 1.45.
     * @return Calculated person's daily eat norm in cal.
     */
    fun getDailyEatNorm(age: Int, height: Int, weight: Int, activityCoefficient: Float): Int {
        return ((360.785 + 11.675 * weight + 3.425 * height - 5.715 * age) * activityCoefficient).roundToInt()
    }
}