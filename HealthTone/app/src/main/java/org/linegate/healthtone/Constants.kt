package org.linegate.healthtone

object Constants {
    val actionStartForegroundService = "ACTION_START_FOREGROUND_SERVICE"
    val foregroundNotificationChannelID = "StepsServiceChannelID"
    val foregroundNotificationChannelName = "Steps service"
}