package org.linegate.healthtone

import android.app.*
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener2
import android.hardware.SensorManager
import android.os.Binder
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.lifecycle.ViewModelProvider
import org.linegate.healthtone.appviewmodel.AppViewModel
import kotlin.math.roundToInt

class StepsService : Service(), SensorEventListener2 {

    companion object {
        var isRunning = false
    }

    inner class StepsServiceBinder : Binder() {
        fun getService(): StepsService = this@StepsService
    }

    private lateinit var sensorManager: SensorManager

    private var previousSteps = 0

    private var firstStepCount = true

    val binder = StepsServiceBinder()

    var appViewModel: AppViewModel? = null

    private fun requireViewModel(): AppViewModel {
        return appViewModel!!
    }

    fun createViewModel() {
        appViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(application).create(AppViewModel::class.java)
    }

    override fun onCreate() {
        super.onCreate()

        createViewModel()

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val stepsSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)
        sensorManager.registerListener(this, stepsSensor, SensorManager.SENSOR_DELAY_FASTEST)

        isRunning = true
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.action) {
            Constants.actionStartForegroundService -> {
                val channel = NotificationChannel(Constants.foregroundNotificationChannelID, Constants.foregroundNotificationChannelName, NotificationManager.IMPORTANCE_LOW)
                (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(channel)

                val notificationIntent = Intent(this, MainActivity::class.java)
                notificationIntent.action = MainActivity::class.java.name
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                val notificationPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                    PendingIntent.FLAG_IMMUTABLE)
                startForeground(1, NotificationCompat.Builder(this, Constants.foregroundNotificationChannelID)
                    .setContentTitle(getString(R.string.s_in_background, getString(R.string.app_name)))
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentIntent(notificationPendingIntent)
                    .build())
            }
        }
        return START_STICKY
    }

    override fun onSensorChanged(event: SensorEvent?) {
        val newSteps = event!!.values[0].roundToInt()
        if (!firstStepCount) {
            requireViewModel().onTimeUpdate()
            requireViewModel().addTodaySteps(newSteps - previousSteps)
        } else {
            firstStepCount = false
        }
        previousSteps = newSteps
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

    override fun onFlushCompleted(sensor: Sensor?) {

    }

    override fun onDestroy() {
        isRunning = false
        super.onDestroy()
    }
}