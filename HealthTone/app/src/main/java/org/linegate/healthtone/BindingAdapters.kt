package org.linegate.healthtone

import android.util.Log
import android.widget.Button
import android.widget.ImageView
import androidx.core.view.doOnDetach
import androidx.core.view.doOnPreDraw
import androidx.databinding.BindingAdapter
import com.db.williamchart.data.Scale
import com.db.williamchart.view.BarChartView
import com.db.williamchart.view.HorizontalBarChartView

@BindingAdapter("imageFromResource")
fun bindImageFromResource(imageView: ImageView, imageResource: Int?) {
    imageResource?.let {
        imageView.setImageResource(imageResource)
    }
}

@BindingAdapter("color")
fun bindColor(imageView: ImageView, color: Int?) {
    color?.let {
        imageView.setColorFilter(color)
    }
}

@BindingAdapter("color")
fun bindColor(button: Button, color: Int?) {
    color?.let {
        button.setBackgroundColor(color)
    }
}

@BindingAdapter("color")
fun bindColor(horizontalBarChartView: HorizontalBarChartView, color: Int?) {
    color?.let {
        horizontalBarChartView.resetPivot()
        horizontalBarChartView.barsColor = color
    }
}

@BindingAdapter("backgroundColor")
fun bindBackgroundColor(horizontalBarChartView: HorizontalBarChartView, color: Int?) {
    color?.let {
        horizontalBarChartView.resetPivot()
        horizontalBarChartView.barsColor = color
        horizontalBarChartView.scale = Scale(0f, 1f)
        horizontalBarChartView.show(listOf("" to 0f, "" to 1f))
    }
}

@BindingAdapter("bar")
fun bindBar(horizontalBarChartView: HorizontalBarChartView, bar: Pair<Float, Float>?) {
    bar?.let {
        horizontalBarChartView.scale = Scale(0f, bar.second)
        horizontalBarChartView.show(listOf("" to 0f, "" to bar.first))
    }
}

@BindingAdapter("bars")
fun bindBars(chartView: BarChartView, bars: List<Pair<String, Float>>?) {
    bars?.let {
        //chartView.scale = Scale(0f, bars.last().second)
        chartView.animate(bars)
    }
}

@BindingAdapter("colors")
fun bindColors(chartView: BarChartView, colors: List<Int>?) {
    colors?.let {
        chartView.resetPivot()
        chartView.barsColorsList = colors
    }
}

@BindingAdapter("barsBackgroundColor")
fun bindBarsBackgroundColor(chartView: BarChartView, color: Int?) {
    color?.let {
        chartView.resetPivot()
        chartView.barsBackgroundColor = color
    }
}