package org.linegate.healthtone

import android.text.InputFilter
import android.text.Spanned

class NotFirstNullInputFilter : InputFilter {
    override fun filter(
        source: CharSequence?,
        start: Int,
        end: Int,
        dest: Spanned?,
        dstart: Int,
        dend: Int
    ): CharSequence {
        source?.let {
            if (start != end && dstart == 0 && it[start] == '0') {
                return ""
            }
        }
        return source ?: ""
    }
}